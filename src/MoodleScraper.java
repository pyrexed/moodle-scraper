/** Created by Roy on 24/05/15.*/


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

public class MoodleScraper {

    private static final String LOGIN_URL = "https://moodle.technion.ac.il/login/index.php";
    private static final String MAIN_PAGE = "http://moodle.technion.ac.il/";
    private HttpsClient session;
    public String base_dir = "C:\\Moodle_Scraper\\";

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            print("usage: supply username (ID num), password and directory:\n" +
                    "java -jar Moodle_scraper 212345678 12345678 C:\\MS");
            return;
        }
        String username = args[0];
        String password = args[1];
        String temp_dir = args[2];
        try {
            MoodleScraper scraper = new MoodleScraper();
            scraper.base_dir = temp_dir;
            print("Fetching %s...", MAIN_PAGE);
            scraper.createSession(username, password);
            String result = scraper.session.GetPageContent(MAIN_PAGE);
            Document doc = Jsoup.parse(result, MAIN_PAGE);
            HashMap<String,URL> courses = scraper.getCourses(doc);
            if (courses.isEmpty()){
                return;
            }
            print("Choose course number, or type 'a' for all course, type 'q' to quit");
            Scanner scan = new Scanner(System.in);
            String input = scan.nextLine();
            while (!input.equals("q")){
                if (input.equals("a")){
                    scraper.processCourses(courses);
                    break;
                } else if (courses.containsKey(input)){
                    Map<String,URL> course = new HashMap<>();
                    course.put(input, courses.get(input));
                    scraper.processCourses(course);
                }
                print("Type a course which is in the list or 'a' or 'q' to quit");
                input = scan.nextLine();
            }



        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void createSession(String username, String pw) throws Exception {
        HttpsClient ses = new HttpsClient();
        // make sure cookies is turn on
        CookieHandler.setDefault(new CookieManager());

        // 1. Send a "GET" request, so that you can extract the form's data.
        String page = ses.GetPageContent(LOGIN_URL);
        String postParams = ses.getFormParams(page, username, pw);

        // 2. Construct above post's content and then send a POST request for
        // authentication
        ses.sendPost(LOGIN_URL, postParams);
        session = ses;
    }

    private HashMap<String, URL> getCourses(Document doc) {
        Elements courses = doc.getElementsByClass("coursename");
        HashMap<String, URL> mapp = new HashMap<>();
        if (courses.isEmpty()){
            print("Could not login check the credentials and try again.");
            return mapp;
        }
        for (Element src : courses) {
            Elements links = src.select("a[href]");
            for (Element link : links) {
                String link_address = link.attr("href");
                try {
                    Pattern regex = Pattern.compile("\\D(\\d{6})\\D");
                    Matcher regexMatcher = regex.matcher(link.toString());
                    while (regexMatcher.find()) {
                        try {
                            URL temp_url = new URL(link_address);
                            mapp.put(regexMatcher.group(1), temp_url);
                            print("[%s] -> %s", regexMatcher.group(1), link_address);
                        }
                        catch (MalformedURLException ex){
                            // error creating link
                        }
                    }
                } catch (PatternSyntaxException ex) {
                    // Syntax error in the regular expression
                }
            }
        }
        return mapp;
    }

    private void processCourses(Map<String,URL> courses){
        for (Map.Entry<String,URL> entry : courses.entrySet())
        {
            try {
                String page_content = session.GetPageContent(entry.getValue().toString());
                Document doc = Jsoup.parse(page_content);
                // dealing with regular resources
                Elements resources = doc.getElementsByClass("resource");
                HashMap<String,URL> resource_links = new HashMap<>();
                for (Element ele : resources){
                    try {
                        Element temp_link = ele.select("a[href]").first();
                        URL link = new URL(temp_link.attr("href"));
                        AbstractMap.SimpleEntry<String, URL> resource_full_link = resolveLink(link);
                        resource_links.put(resource_full_link.getKey(), resource_full_link.getValue());
                    }
                    catch (MalformedURLException ex){
                        print("Error in parsing link");
                    }
                }

                // dealing with assignments (homework)
                Elements assignments = doc.getElementsByClass("assign");
                HashMap<String,URL> assignments_links = new HashMap<>();
                for (Element ele : assignments){
                    try {
                        Element temp_link = ele.select("a[href]").first();
                        URL link = new URL(temp_link.attr("href"));
                        assignments_links.putAll(resolveAssign(link));
                    }
                    catch (MalformedURLException ex){
                        print("Error in parsing link");
                    }
                }
                resource_links.putAll(assignments_links);

                // dealing with subfolders
                Elements subs = doc.getElementsByClass("folder");
                HashMap<String,URL> sub_links = new HashMap<>();
                for (Element ele : subs){
                    try {
                        Element temp_link = ele.select("a[href]").first();
                        URL link = new URL(temp_link.attr("href"));
                        sub_links.putAll(resolveSub(link));
                    }
                    catch (MalformedURLException ex){
                        print("Error in parsing link");
                    }
                }
                resource_links.putAll(sub_links);
                downloadResources(resource_links, entry.getKey());
                print("Done with: %s", entry.getKey());
            }
            catch (Exception e){
                print("error occurred at processCourses: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    private HashMap<String,URL> resolveAssign(URL assignment_link){
        // Resolves a link to an assignments and extracts links to resources
        HashMap<String,URL> resolved = new HashMap<>();
        try {
            Document doc = Jsoup.parse(session.GetPageContent(assignment_link.toString()));
            Elements hw_links = doc.getElementById("intro").select("a[href]");
            for (Element link : hw_links) {
                AbstractMap.SimpleEntry<String, URL> res = resolveLink(new URL(link.attr("href")));
                resolved.put(res.getKey(), res.getValue());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return resolved;
    }

    private HashMap<String,URL> resolveSub(URL sub_link){
        // Resolves a link to an sub-directories and extracts links to resources
        HashMap<String,URL> resolved = new HashMap<>();
        try {
            Document doc = Jsoup.parse(session.GetPageContent(sub_link.toString()));
            Elements sub_links = doc.getElementsByClass("filemanager").select("a[href]");
            for (Element link : sub_links){
                AbstractMap.SimpleEntry<String, URL> res = resolveLink(new URL(link.attr("href")));
                resolved.put(res.getKey(), res.getValue());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return resolved;
    }

    private void downloadFromUrl(URL url, String localFilename) throws IOException {
        InputStream is = null;
        FileOutputStream fos = null;

        try {
            URLConnection urlConn = url.openConnection();//connect

            is = urlConn.getInputStream();               //get connection inputstream
            fos = new FileOutputStream(localFilename);   //open outputstream to local file

            byte[] buffer = new byte[4096];              //declare 4KB buffer
            int len;

            //while we have available data, continue downloading and storing to local file
            while ((len = is.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } finally {
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }
    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private void downloadResources(HashMap<String,URL> resources, String coursenum){
        File path = new File(base_dir, coursenum);
        if (!path.exists()) {
            if (path.mkdirs()) {
                print("Directory is created: %s", path.getAbsolutePath());
            } else {
                print("Failed to create directory: %s", path.getAbsolutePath());
            }
        }
        for (HashMap.Entry<String,URL> resource : resources.entrySet()){
            File dfile = new File(path, resource.getKey());
            while (dfile.exists()){
                String temp_name = dfile.getName();
                String[] temp_array = temp_name.split("\\.");
                File temp_file = new File(path.getAbsolutePath(), temp_array[0] + "_." + temp_array[1]);
                dfile.renameTo(temp_file);
            }
            try {
                downloadFromUrl(resource.getValue(), dfile.getAbsolutePath());
            }
            catch (IOException ex){
                print("Failed to download %s", resource.getKey());
                ex.printStackTrace();
            }
        }
    }

    private AbstractMap.SimpleEntry<String,URL> resolveLink(URL link){
        AbstractMap.SimpleEntry<String,URL> ret = new AbstractMap.SimpleEntry<>(null,null);
        try {
            URL full_link;
            String name = null;
            String html  = session.GetPageContent(link.toString());
            Document doc = Jsoup.parse(html);
            // method 1: finding the link in the html source code
            Element temp = doc.getElementsByClass("resourceworkaround").select("a[href]").first();
            if (temp == null) {
                temp = doc.getElementsByClass("resourcecontent").select("a[href]").first();
            }
            if (temp != null) {
                full_link = new URL(temp.attr("href"));
                name = temp.childNode(0).toString();
            }
            // method 2: finding the link in the redirection
            else {
                full_link = session.getRedirect();
                Pattern regex = Pattern.compile(".+/(.+?\\.[\\w\\d]{1,4})");
                Matcher regexMatcher = regex.matcher(full_link.toString());
                while (regexMatcher.find()) {
                    name = regexMatcher.group(1);
                }
            }
            ret = new AbstractMap.SimpleEntry<>(name,full_link);
        }
        catch (Exception ex){
            //TODO: check for more cases especially in 096250, and one in 096411 and 094189
            print("Error occurred in resolveLink: %s", link.toString());
            ex.printStackTrace();
        }
        return ret;
    }
}
