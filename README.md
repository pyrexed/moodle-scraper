**Requirements**: **Java 8 and above.**

Tested only on windows 64-bit and ubuntu 15.10/amd64.


**Download it from** [HERE](https://bitbucket.org/pyrexed/moodle-scraper/downloads/Moodle_scraper.jar)

**Usage**:

Launch CMD or terminal (if your in linux) in the directory the file is located.
Enter the Following command, and follow the instructions on screen:

java -jar Moodle_scraper.jar <username> <password> <directory>



Example:
Java -jar Moodle_scraper.jar 212345678 12345678 C:\MS